FROM node:8.11.3

RUN mkdir /home/app/
WORKDIR /home/app

COPY package.json yarn.lock /home/app/
RUN yarn install
COPY . .
